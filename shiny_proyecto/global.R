#install.packages("viridis")
## global.R ##
library(readxl)
library(dplyr)
library(lubridate)
library(feather)
#devtools::install_github("KevinPedroza/lucr", force = TRUE)
#library(lucr)
library(DT)
library(treemap)
library(hrbrthemes)
library(ggplot2)
library(viridis)
library(plotly)



#abonos<- read_excel("datos/abonos.xlsx",sheet = "abonos") #se lee el archivo de exel
#write_feather(abonos, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/abonos.feather") #convierte el archivo ventas a un .feather
abonos <-read_feather("datos/abonos.feather") #se lee el nuevo archivo feather 
#View(abonos) 

#creditos<- read_excel("datos/creditos.xlsx",sheet = "creditos") #se lee el archivo de exel
#write_feather(creditos, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/creditos.feather") #convierte el archivo ventas a un .feather
creditos <-read_feather("datos/creditos.feather") #se lee el nuevo archivo feather 
#View(creditos) 

#Se creo inner join entre la variable abonos y creditos
abo_vent <- inner_join (abonos, creditos, by = "CODIGO_CREDITO") 
#View(abo_vent)

#Se covierten a un nuevo tipo.
abo_vent$CODIGO_ABONOS<-as.integer(abo_vent$CODIGO_ABONOS)
abo_vent$CODIGO_CREDITO<-as.integer(abo_vent$CODIGO_CREDITO)
abo_vent$MONTO<-as.integer(abo_vent$MONTO)
abo_vent$FECHA<-as.Date(abo_vent$FECHA)
abo_vent$ESTADO_CREDITO<-as.integer(abo_vent$ESTADO_CREDITO)
#glimpse(abo_vent)

# <<<<<<<<<< FINALIZA KPI 1>>>>>>>>>

# <<<<<<<<< INICIA KPI 2 >>>>>>>>>

"filtrar por fecha primero para sacar las fechas acorde al mes seleccionado.
se filtra la nueva variable con las ventas ya filtradas por fecha, para sacar los 5 clientes con mas ventas,
luego se hace un inner join con las tablas clientes y contactos
y por ultimo se saca la infomacion el nombre del cliente y la direccion"

#ventas<- read_excel("datos/ventas.xlsx",sheet = "ventas") #se lee el archivo de exel
#write_feather(ventas, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/ventas.feather") #convierte el archivo ventas a un .feather
ventas <-read_feather("datos/ventas.feather") #se lee el nuevo archivo feather 
ventas$COMPROBANTE<-as.integer(ventas$COMPROBANTE)
ventas$CODIGO_EMPLEADO<-as.integer(ventas$CODIGO_EMPLEADO)
ventas$CODIGO_PRODUCTO<-as.integer(ventas$CODIGO_PRODUCTO)
ventas$CODIGO_CLIENTE<-as.integer(ventas$CODIGO_CLIENTE)
ventas$CANTIDAD_PRODUCTO<-as.integer(ventas$CANTIDAD_PRODUCTO)
ventas$TIPO_VENTA<-as.integer(ventas$TIPO_VENTA)
ventas$MONTO<-as.integer(ventas$MONTO)
ventas$FECHA<-as.Date(ventas$FECHA)

#clientes<- read_excel("datos/clientes.xlsx",sheet = "clientes") #se lee el archivo de exel
#write_feather(clientes, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/clientes.feather") #convierte el archivo ventas a un .feather
clientes <-read_feather("datos/clientes.feather") #se lee el nuevo archivo feather 
clientes$IDENTIFICACION<-as.integer(clientes$IDENTIFICACION)
clientes$CODIGO_CLIENTE<-as.integer(clientes$CODIGO_CLIENTE)
clientes$NOMBRE_COMPLETO<-as.character(clientes$NOMBRE_COMPLETO)
#glimpse(clientes)

#contactos<- read_excel("datos/contactos.xlsx",sheet = "contactos") #se lee el archivo de exel
#write_feather(contactos, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/contactos.feather") #convierte el archivo ventas a un .feather
contactos <-read_feather("datos/contactos.feather") #se lee el nuevo archivo feather 
colnames(contactos)<-c("IDENTIFICACION","TELEFONO","CORREO","DIRECCION")
contactos$IDENTIFICACION<-as.integer(contactos$IDENTIFICACION)
contactos$TELEFONO<-as.numeric(contactos$TELEFONO)
contactos$CORREO<-as.character(contactos$CORREO)
contactos$DIRECCION<-as.character(contactos$DIRECCION)

clien_contac <- inner_join (clientes, contactos, by = "IDENTIFICACION")

clientes_2<-clientes
colnames(clientes_2)<-c("CODIGO","CODIGO_CLIENTE","NOMBRE_COMPLETO")
vent_clien <- inner_join(ventas, clientes_2, by = "CODIGO_CLIENTE")
#View(clien_contac)

# <<<<<<<<<< FINALIZA KPI 2 >>>>>>>>>

# <<<<<<<<<< INICIA KPI 4 >>>>>>>>>

#compras<- read_excel("datos/compras.xlsx",sheet = "compras") #se lee el archivo de exel
#write_feather(compras, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/compras.feather") #convierte el archivo ventas a un .feather
compras <-read_feather("datos/compras.feather") #se lee el nuevo archivo feather 

compras$CODIGO_COMPRAS<-as.integer(compras$CODIGO_COMPRAS)
compras$CODIGO_PROVEEDOR<-as.integer(compras$CODIGO_PROVEEDOR)
compras$CODIGO_PRODUCTO<-as.integer(compras$CODIGO_PRODUCTO)
compras$CANTIDAD<-as.integer(compras$CANTIDAD)
compras$IVA<-as.double(compras$IVA)
compras$TOTAL_COMPRA<-as.integer(compras$TOTAL_COMPRA)
compras$FECHA<-as.Date(compras$FECHA)

#proveedores<- read_excel("datos/proveedores.xlsx",sheet = "proveedores") #se lee el archivo de exel
#write_feather(proveedores, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/proveedores.feather") #convierte el archivo ventas a un .feather
proveedores<-read_feather("datos/proveedores.feather") #se lee el nuevo archivo feather

proveedores$CODIGO_PROVEEDOR<-as.integer(proveedores$CODIGO_PROVEEDOR)
proveedores$NOMBRE_PROVEEDOR<-as.character(proveedores$NOMBRE_PROVEEDOR)
proveedores$TELEFONO<-as.numeric(proveedores$TELEFONO)
proveedores$CORREO<-as.character(proveedores$CORREO)

compra_proveed<-inner_join (compras, proveedores, by = "CODIGO_PROVEEDOR")

# <<<<<<<<<< FINALIZA KPI 4 >>>>>>>>>

# <<<<<<<<<< INICIA KPI 3 >>>>>>>>>

#productos<- read_excel("datos/productos.xlsx",sheet = "productos") #se lee el archivo de exel
#write_feather(productos, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/productos.feather") #convierte el archivo ventas a un .feather
productos <-read_feather("datos/productos.feather") #se lee el nuevo archivo feather 
#View(productos)
colnames(productos)<-c("CODIGO_PRODUCTO","NOMBRE_PRODUCTO","IVA","PRECIO_PRODUCTO","CODIGO_PROVEEDOR","CANTIDAD")

vent_prod <- inner_join (ventas, productos, by = "CODIGO_PRODUCTO")
vent_prod <- inner_join(vent_prod, proveedores, by = "CODIGO_PROVEEDOR")

# <<<<<<<<<< FINALIZA KPI 3 >>>>>>>>>

# <<<<<<<<<< INICIA KPI 5 >>>>>>>>>

#empleados<- read_excel("datos/empleados.xlsx",sheet = "empleados") #se lee el archivo de exel
#write_feather(empleados, "C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/empleados.feather") #convierte el archivo ventas a un .feather
empleados<-read_feather("datos/empleados.feather") #se lee el nuevo archivo feather

empleados$CODIGO_EMPLEADO<-as.integer(empleados$CODIGO_EMPLEADO)
empleados$NOMBRE_COMPLETO<-as.character(empleados$NOMBRE_COMPLETO)
empleados$DIRECCION<-as.character(empleados$DIRECCION)
empleados$TELEFONO<-as.numeric(empleados$TELEFONO)
empleados$CORREO<-as.character(empleados$CORREO)
empleados$CODIGO_SALARIO<-as.integer(empleados$CODIGO_SALARIO)

vent_emplea<-inner_join(ventas, empleados, by = "CODIGO_EMPLEADO")

# <<<<<<<<<< FINALIZA KPI 5 >>>>>>>>>


# Funcion formato colones -------------------------------------------------
colones <- function(x) {
  paste0("₡ ",format(round(as.numeric(x), 1), nsmall=1, big.mark=","))
}