library(shiny)
library(shinydashboard)
library(dplyr)
library(ggplot2)


header <- dashboardHeader(
  title = "Proyecto"
)

## Menu a la izquierda----------------------------------------------------------
sidebar <- dashboardSidebar(
  sidebarMenu(
    menuItem("Creditos y Abonos", tabName = "KPI_1", icon = icon("glyphicon glyphicon-stats", lib = "glyphicon")), # From glyphicon library,
    menuItem("Clientes con mayores compra", tabName = "KPI_2", icon = icon("glyphicon glyphicon-stats", lib = "glyphicon")),
    menuItem("Producto más vendido", tabName = "KPI_3", icon = icon("glyphicon glyphicon-stats", lib = "glyphicon")),
    menuItem("Compras a proveedores", tabName = "KPI_4", icon = icon("glyphicon glyphicon-stats", lib = "glyphicon")),
    menuItem("Ventas por empleado", tabName = "KPI_5", icon = icon("glyphicon glyphicon-stats", lib = "glyphicon"))
  )
)


## Contenido -------------------------------------------------------------------
body <- dashboardBody(
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "style1.css")),
  
  tabItems(
    tabItem(tabName = "KPI_1",
            
            h2("Creditos y Abonos"),
            
            # SELECT
            fluidRow(
              box(width = 4, collapsible = TRUE,selectInput("año", h3("Seleccione el año"),
                                                            choices=unique(format(abo_vent$FECHA,"%Y")))  
              )
            ),
            # CAJAS
            fluidRow(
              valueBoxOutput("total_abonos",width = 3),
              valueBoxOutput("total_creditos",width = 3)
            ),
            fluidRow(
              h3("Tasa de Crecimiento de Creditos"),
              valueBoxOutput("crecimiento1",width = 3),
              valueBoxOutput("crecimiento2",width = 3)
            ),
            fluidRow(
              plotlyOutput("crecimiento",height="500px", width = "90%")
            )
            # TABLA
            #fluidRow(
            #  column(5, DT::dataTableOutput("tabla_creditos")
            #))
    ),
    
    tabItem(tabName = "KPI_2",
            
            h2("Clientes con mayores compras"),
            
            fluidRow(
              box(width = 2, collapsible = TRUE,selectInput("mes", h3("Seleccione el mes"),
                                                            choices=unique(format(ventas$FECHA,"%m")))  
              ),
              box(width = 2, collapsible = TRUE,selectInput("anno2", h3("Seleccione el año"),
                                                            choices=unique(format(ventas$FECHA,"%Y")))  
              )
            ),
            fluidRow(
              column(width = 8, DT::dataTableOutput("tbl_clientes"))
            ),
            fluidRow(
              box(width = 8, collapsible = TRUE,
                  title = "Compras por clientes",
                  status = "danger",
                  plotOutput("cantida_compras")
              )
            )
    ),
    
    tabItem(tabName = "KPI_3",
            h2("El producto más vendido mensualmente"),
            fluidRow(
              box(width = 2, collapsible = TRUE, dateRangeInput('rangofechas',
                             label = "Rango de Fechas",
                             start = min(vent_prod$FECHA), end = max(vent_prod$FECHA),
                             separator = " - ", format = "dd/mm/yyyy",
                             startview = 'year', language = 'es', weekstart = 1)
              ),
              box(width = 2, collapsible = TRUE,radioButtons("buscardor", "Buscar productos por:",
                                                             c("Fechas"=TRUE, "Fechas y proveedor"=FALSE))),
              box(width = 3, collapsible = TRUE,selectInput("proveedor13", h3("Seleccionar proveedor"),
                                                            choices=unique(vent_prod$NOMBRE_PROVEEDOR))  
              ),
            ),
            fluidRow(
              plotOutput("threemap_product",height="500px", width = "90%")
            )
    ),
    tabItem(tabName = "KPI_4" ,
            h2("Compras realizadas a proveedores mensualmente"),
            fluidRow(
              box(width = 2, collapsible = TRUE,selectInput("mes4", h3("Seleccione el mes"),
                                                            choices=unique(format(compra_proveed$FECHA,"%m")))  
              ),
              box(width = 2, collapsible = TRUE,selectInput("anno4", h3("Seleccione el año"),
                                                            choices=unique(format(compra_proveed$FECHA,"%Y")))  
              ),
              box(width = 2, collapsible = TRUE,uiOutput("provee_select")),
              box(width = 2, collapsible = TRUE,radioButtons("tipografico", "Tipo de Grafico",
                                                             c("Lineal"=TRUE, "Puntos"=FALSE))),
            ),
            fluidRow(
              plotlyOutput("compra_provee",height="500px", width = "90%")
            ),
            fluidRow(
              plotOutput("provee_chart", height = "400px", width = "90%")
            )
    ),
    tabItem(tabName = "KPI_5" ,
            h2("Cantidad de ventas realizadas por empleado"),
            fluidRow(
              box(width = 2, collapsible = TRUE,selectInput("empleado5", h3("Seleccione el empleado"),
                                                            choices=unique(vent_emplea$CODIGO_EMPLEADO))  
              ),
              box(width = 2, collapsible = TRUE, radioButtons("buscar_emple", "Tipo busqueda",
                                                             c("Por empleado"= "v", "Empleado más ventas al año"= "f"))),
              valueBoxOutput("nombre_empleado"),
            ),
            fluidRow(
              plotlyOutput("vent_emple",height="500px", width = "90%")
            )
    )
    
  )
  
)


## App completo ----------------------------------------------------------------
dashboardPage(
  header,
  sidebar,
  body,
  skin = "red"
)