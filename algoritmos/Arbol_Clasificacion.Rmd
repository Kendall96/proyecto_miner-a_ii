---
title: "Arbol Clasificacion"
author: "Kevin y Kendall"
date: "12/1/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


## Aca se traen las librerias necesarias para los procesos
```{r}
library(rpart)      # Algoritmo árbol de decisión
library(rpart.plot) # Graficar árbol de decisión
library(C50)  # Paquete que contiene dataset Churn, algoritmo árboles de decisión...
library(readxl) # Paquete para leer archivos .feather
library(dplyr)
```


## Aca se traen los datos, además de que realiza un JOIN de Clientes y Productos
```{r}
ventas <- read_xlsx("/Users/kevinpedroza/Documents/proyecto_miner-a_ii/algoritmos/datos/ventas.xlsx")
clientes <- read_xlsx("/Users/kevinpedroza/Documents/proyecto_miner-a_ii/algoritmos/datos/clientes.xlsx")
productos <- read_xlsx("/Users/kevinpedroza/Documents/proyecto_miner-a_ii/algoritmos/datos/productos.xlsx")
#ventas <- read_feather("C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/ventas.feather")
#clientes <- read_feather("C:/Practicas con R/proyecto_miner-a_ii/shiny_proyecto/datos/clientes.feather")

ventas <- inner_join (ventas, clientes, by = c("CODIGO_CLIENTE" = "IDENTIFICACION"))
ventas <- inner_join (ventas, productos, by = c("CODIGO_PRODUCTO" = "CODIGO_PRODUCTO"))
ventas <- ventas[,c(3,5,6,7,9,10,12,13,18)]

```


## Aca vamos a preparar los datos que se utilizan tanto para entrenamiento y prueba
```{r}
#### PASO 3: Creamos set de entrenamiento y de test  
# Dividimos el dataset en 60% entreno y 40% validación
# con sample creamos dos grupos con 1 y 2 
ind        <- sample(2,nrow(ventas), replace=TRUE, prob=c(0.6, 0.4)) # 60% entrenamiento, 40 % test
trainData  <- ventas[ind==1, ] # Entrenamiento
testData   <- ventas[ind==2, ] # Test
```

## Aca se crea el arból de decisión
```{r}
#### PASO 4: Creamos árbol de decisión   
# cno Metod decimos que la variable dependiente es de cualitativa
ArbolRpart <- rpart(TIPO_VENTA ~ ., method = "class", data = trainData)

```


## Aca revisamos el gráfico y la información del arból
```{r}
#### PASO 5: Revisamos información y gráfico    
# de 2900 observaciones  el 86 no se va y el 13 si se va y son 4050 observaciones
print(ArbolRpart)                         
rpart.plot(ArbolRpart,extra=4)  # extra=4:probabilidad de observaciones por clase
printcp(ArbolRpart)             # estadísticas de resultados
plotcp(ArbolRpart)              # evolución del error a medida que se incrementan los nodos

```

## Aca limpiamos la informacion que presenta error en el arból 
```{r}
# Podado del árbol
# cortamos con el XERROR menor encontrado o ingresamos el cp deseados manualmente
pArbolRpart<- prune(ArbolRpart, cp= ArbolRpart$cptable[which.min(ArbolRpart$cptable[,"xerror"]),"CP"])
pArbolRpart<- prune(ArbolRpart, cp= 0.015079)
printcp(pArbolRpart)

```


## Aca se predice el tipo de venta en información de prueba
```{r}
#### PASO 6: Predice el tipo de venta en testData 
# Validamos la capacidad de predicción del árbol con el fichero de validación
# con los datos del entrenamiento
testPredRpart <- predict(ArbolRpart, newdata = testData, type = "class")
```


## Aca se visualiza la información del arból
```{r}
# Visualizamos una matriz de confusión
table(testPredRpart, testData$TIPO_VENTA)
View(testPredRpart)

```


## Aca se realiza la estadistica del modelo, y se muestra el proceso para realizarlo
```{r}
#### PASO 7: Estadística del modelo  
# Calculamos el % de aciertos
#suman los aciertos y se divide entre el numero de predicciones
sum(testPredRpart == testData$TIPO_VENTA) / length(testData$TIPO_VENTA)*100
```


